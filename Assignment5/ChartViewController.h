/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the priceTag model that is used to hold the data for each calculation, it returns original price and discount price
 */


#import <UIKit/UIKit.h>
#import "MyQuartzView.h"

@interface ChartViewController : UIViewController

@property(strong, nonatomic) NSDecimalNumber * tempTotalPrice;
@property(strong, nonatomic) NSDecimalNumber * tempDiscountPrice;


@end
