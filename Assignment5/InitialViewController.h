/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the initial view controller, which takes care of user input in the textfields and calculation of each price tag
 */

#import <UIKit/UIKit.h>
#import "PriceTag.h"

@interface InitialViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) UIScrollView * view;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *dollarsOffTextField;
@property (weak, nonatomic) IBOutlet UITextField *discountTextField;
@property (weak, nonatomic) IBOutlet UITextField *additionalDiscountTextField;
@property (weak, nonatomic) IBOutlet UITextField *taxTextField;
@property (weak, nonatomic) IBOutlet UILabel *originalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *calculateButton;
@property (weak, nonatomic) UITextField * activeTextField;
- (IBAction)calculateDiscount:(id)sender;

@end
