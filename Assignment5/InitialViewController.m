/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the initial view controller, which takes care of user input in the textfields and calculation of each price tag
 */


#import "NextTextFieldProperty.h"
#import "PreviousTextFieldProperty.h"
#import "InitialViewController.h"
#import "ChartViewController.h"
#import "MyQuartzView.h"

@interface InitialViewController ()

@end

@implementation InitialViewController

@synthesize dollarsOffTextField;
@synthesize additionalDiscountTextField;
@synthesize discountTextField;
@synthesize discountPriceLabel;
@synthesize originalPriceLabel;
@synthesize priceTextField;
@synthesize taxTextField;
@synthesize calculateButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Initial: %p", self);
    
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor grayColor] forKey:NSForegroundColorAttributeName];
    
    
    
    /* Setting Text fields next */
    priceTextField.nextTextField = dollarsOffTextField;
    dollarsOffTextField.nextTextField = discountTextField;
    discountTextField.nextTextField = additionalDiscountTextField;
    additionalDiscountTextField.nextTextField = taxTextField;
    taxTextField.nextTextField = priceTextField;
    
    /* Setting Text fields previous */
    priceTextField.previousTextField = taxTextField;
    taxTextField.previousTextField = additionalDiscountTextField;
    additionalDiscountTextField.previousTextField = discountTextField;
    discountTextField.previousTextField = dollarsOffTextField;
    dollarsOffTextField.previousTextField = priceTextField;
    
    /* Create a tap gesture recognizer and assign it to the scrollable view */
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self registerForKeyboardNotifications];
    [self.view addGestureRecognizer:tapGesture];
    
    calculateButton.enabled = NO;
    
}

/* REFERENCE: https://developer.apple.com/library/ios/documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html  */

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    UIScrollView * tempScroll = (UIScrollView *) self.view;
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    tempScroll.contentInset = contentInsets;
    tempScroll.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, CGPointMake(self.activeTextField.frame.origin.x, (self.activeTextField.frame.origin.y+ 20)) ))
    {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y - ((keyboardSize.height)));
        [tempScroll setContentOffset:scrollPoint animated:YES];
    }
    /* If the text field doesn't hide the keyboard scroll the view to its default position */
    else
    {
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.frame.origin.y + (aRect.origin.y - self.activeTextField.frame.origin.y));
        [tempScroll setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIScrollView * tempScroll = (UIScrollView *) self.view;
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    tempScroll.contentInset = contentInsets;
    tempScroll.scrollIndicatorInsets = contentInsets;
    
    
}

-(void)dismissKeyboard
{
    /* Check which textField is the first Responder and resign the FirstResponder */
    if(![self isFirstResponder])
    {
        if ([priceTextField isFirstResponder])
        {
            [priceTextField resignFirstResponder];
        }
        if ([dollarsOffTextField isFirstResponder])
        {
            [dollarsOffTextField resignFirstResponder];
        }
        if ([discountTextField isFirstResponder])
        {
            [discountTextField resignFirstResponder];
        }
        if ([additionalDiscountTextField resignFirstResponder])
        {
            [additionalDiscountTextField resignFirstResponder];
        }
        if ([taxTextField isFirstResponder])
        {
            [taxTextField resignFirstResponder];
        }
        
    }
    
    
}

/* Function called when Next Button on the Keyboard Toolbar is pressed, will move to next textfield */
-(void)nextClicked: (UIBarButtonItem * )sender
{
    UITextField* next = self.activeTextField.nextTextField;
    if (next) {
        [next becomeFirstResponder];
    }
}

/* Function called when Previous Button on the Keyboard Toolbar is pressed, will move the previous textfield */
-(void)previousClicked: (UIBarButtonItem * )sender
{
    UITextField* previous = self.activeTextField.previousTextField;
    if (previous) {
        [previous becomeFirstResponder];
    }
}

/* Function called when Done Button on the Keyboard Toolbar is pressed */
- (void) doneClicked: (UIBarButtonItem*) sender
{
    /* Resign the keybarod */
    [self.activeTextField resignFirstResponder];
    
}




- (BOOL)textFieldShouldBeginEditing: (UITextField *) textField
{
    
    /* Set the textfield being edited as active textfield */
    self.activeTextField = textField;
    
    /* Create Keyboard Toolbar with Previous, Next and Done Button */
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar sizeToFit];
    
    UIBarButtonItem *prevButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Previous"
                                   style: UIBarButtonItemStyleDone
                                   target: self
                                   action:@selector(previousClicked:)];
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Next"
                                   style: UIBarButtonItemStyleDone
                                   target: self
                                   action:@selector(nextClicked:)];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
                                   target: self
                                   action: nil];
    
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                  target: self
                                  action: @selector(doneClicked:)];
    
    NSArray* itemsArray = @[prevButton, nextButton, flexButton, doneButton];
    
    [toolbar setItems: itemsArray];
    
    [textField setInputAccessoryView: toolbar];
    
    if (priceTextField.text.length != 0)
    {
        calculateButton.enabled = YES;
    }
    else{
        calculateButton.enabled = NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing: (UITextField *) textField
{
    if (priceTextField.text.length != 0 )
    {
        calculateButton.enabled = YES;
    }
    else{
        calculateButton.enabled = NO;
    }
    
    return YES;
}

-(void)removeCurrencyFormatting:(NSMutableString *)thisString
{
    /* Remove the $ */
    [thisString replaceOccurrencesOfString:@"$"
                                withString:@""
                                   options:NSLiteralSearch
                                     range:NSMakeRange(0, [thisString length])];
    
    /* Remove the , */
    [thisString replaceOccurrencesOfString:@","
                                withString:@""
                                   options:NSLiteralSearch
                                     range:NSMakeRange(0, [thisString length])];
    
    /* Remove the % */
    [thisString replaceOccurrencesOfString:@"%"
                                withString:@""
                                   options:NSLiteralSearch
                                     range:NSMakeRange(0, [thisString length])];
}

- (IBAction)calculateDiscount:(id)sender
{
    PriceTag * thePriceTag = [PriceTag priceTag];
    
    /* Price */
    NSMutableString *temp = [NSMutableString stringWithString:[priceTextField text]];
    
    if(temp.length == 0)
    {
        [temp setString:@"0.00"];
    }
    else{
        [self removeCurrencyFormatting:temp];
    }
    
    [thePriceTag setPrice:[NSDecimalNumber decimalNumberWithString:temp]];
    
    /* Dollars off */
    temp = [NSMutableString stringWithString:[dollarsOffTextField text]];
    
    if(temp.length == 0)
    {
        [temp setString:@"0.00"];
    }
    else{
        [self removeCurrencyFormatting:temp];
    }
    
    [thePriceTag setDollarsOff:[NSDecimalNumber decimalNumberWithString:temp]];
    
    /* Discount Percent */
    temp = [NSMutableString stringWithString:[discountTextField text]];
    
    if(temp.length == 0)
    {
        [temp setString:@"0"];
    }
    else{
        [self removeCurrencyFormatting:temp];
    }
    
    [thePriceTag setDiscountPercent:[NSDecimalNumber decimalNumberWithString:temp]];
    
    /* Additional Discount Percent */
    temp = [NSMutableString stringWithString:[additionalDiscountTextField text]];
    
    if(temp.length == 0)
    {
        [temp setString:@"0"];
    }
    else{
        [self removeCurrencyFormatting:temp];
    }
    
    [thePriceTag setAdditionalDiscount:[NSDecimalNumber decimalNumberWithString:temp]];
    
    /* Tax */
    temp = [NSMutableString stringWithString:[taxTextField text]];
    
    if(temp.length == 0)
    {
        [temp setString:@"0"];
    }
    else{
        [self removeCurrencyFormatting:temp];
    }
    
    [thePriceTag setTax:[NSDecimalNumber decimalNumberWithString:temp]];
    
    
    
    /* Calculate original price and discounted price */
    
    /* Currency Formatter */
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    [originalPriceLabel setText:[currencyFormatter stringFromNumber: [thePriceTag originalPrice]]];
    [discountPriceLabel setText:[currencyFormatter stringFromNumber: [thePriceTag discountPrice ]]];
    
    
}


- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string
{
    
    if (priceTextField.text.length != 0 )
    {
        calculateButton.enabled = YES;
    }
    else{
        calculateButton.enabled = NO;
    }
    
    if (textField == priceTextField || textField == dollarsOffTextField)
    {
        /* Currency Formatter */
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [currencyFormatter setMinimumFractionDigits:2];
        
        NSString *replacementString = [NSString stringWithString:string];
        
        NSMutableString *textFieldText = [NSMutableString stringWithString:[textField text]];
        
        
        /* If character has been deleted, replace the textField text with the string with character deleted */
        [textFieldText replaceCharactersInRange:range withString:replacementString];
        
        /* Remove the $ */
        [textFieldText replaceOccurrencesOfString:@"$"
                                       withString:@""
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [textFieldText length])];
        
        /* Remove the . */
        [textFieldText replaceOccurrencesOfString:@"."
                                       withString:@""
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [textFieldText length])];
        
        /* Remove the , */
        [textFieldText replaceOccurrencesOfString:@","
                                       withString:@""
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [textFieldText length])];
        
        /* If the length is less than 8 */
        if (textFieldText.length <= 8)
        {
            /* Divide value by 100 */
            NSDecimalNumber *textFieldTextNum = [NSDecimalNumber decimalNumberWithString:textFieldText];
            NSDecimalNumber *divideByNum = [[[NSDecimalNumber alloc] initWithInt:10] decimalNumberByRaisingToPower:2];
            NSDecimalNumber *textFieldTextNewNum = [textFieldTextNum decimalNumberByDividingBy:divideByNum];
            /* Format again */
            NSString *textFieldTextNewStr = [currencyFormatter stringFromNumber:textFieldTextNewNum];
            
            /* Change textfield text to new formatted string */
            [textField setText:textFieldTextNewStr];
        }
    }
    else /* If textField is a percentage field */
    {
        
        NSString *replacementString = [NSString stringWithString:string];
        NSMutableString *textFieldText = [NSMutableString stringWithString:[textField text]];
        
        if ([textFieldText isEqualToString:[NSString stringWithFormat:@"0%%"]])
        {
            [textFieldText replaceOccurrencesOfString:@"0"
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [textFieldText length])];
        }
        
        /* Remove the % */
        [textFieldText replaceOccurrencesOfString:@"%"
                                       withString:@""
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [textFieldText length])];

        // If the replacementString is a decimal number
        if ([replacementString  isEqual: @"."])
        {
            //Make sure there is only one decimal
            if([[[textField text] componentsSeparatedByString:@"." ]count] < 2)
            {
                [textFieldText appendString:replacementString];
                
                if (textFieldText.length <= 8)
                {
                    [textField setText:textFieldText];
                    
                }
                
            }

            
            
        }
        else{
            
            
            if ([textFieldText isEqualToString:[NSString stringWithFormat:@"0%%"]])
            {
                [textFieldText replaceOccurrencesOfString:@"0"
                                               withString:@""
                                                  options:NSLiteralSearch
                                                    range:NSMakeRange(0, [textFieldText length])];
            }
            
            /* Remove the % */
            [textFieldText replaceOccurrencesOfString:@"%"
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [textFieldText length])];
            
            /* If character has been deleted, replace the textField text with the string with character deleted */
            if ([replacementString  isEqual: @""])
            {
                if (textFieldText.length != 0)
                {
                    [textFieldText replaceCharactersInRange:NSMakeRange([textFieldText length]-1, 1) withString:replacementString];
                }
            }
            else{
                [textFieldText appendString:replacementString];
            }
            
            
            NSLog(@"TextfieldText: %@", textFieldText);
            
            /* If the textfield have a length of 8 or below */
            if (textFieldText.length <= 8)
            {
                NSString * textFieldTextWithPercentage = [NSString stringWithFormat:@"%@%%", textFieldText];
                
                if ([textFieldText length] == 0)
                {
                    textFieldTextWithPercentage = [NSString stringWithFormat:@"0%%"];
                }
                [textField setText:textFieldTextWithPercentage];
                
            }
            
        }
    }
    
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    PriceTag * thePriceTag = [PriceTag priceTag];
    
    if([segue.identifier isEqualToString:@"toChart"])
    {
        /* Send data to Chart Controller */
        ChartViewController * destinationController = (ChartViewController *)segue.destinationViewController;
        destinationController.tempTotalPrice = [thePriceTag originalPrice];
        destinationController.tempDiscountPrice = [thePriceTag discountPrice];
        
    }
}


@end
