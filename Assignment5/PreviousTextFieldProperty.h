/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the previousTextfield attribute that is being added to UITextField
 */

#import <UIKit/UIKit.h>

@interface UITextField (PreviousTextFieldProperty)

@property (retain, nonatomic) UITextField* previousTextField;

@end
