/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the the chart view controller, which includes populating the total price and discount price properties of the quartz view
 */


#import "ChartViewController.h"

@interface ChartViewController ()

@end

@implementation ChartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Chart: %p", self);
    
    [self.navigationItem setHidesBackButton:YES];
    
    self.navigationItem.title = @"Discount Bar Chart ";
    
    // Do any additional setup after loading the view.
    UISwipeGestureRecognizer* swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self action: @selector(handleSwipeRightFrom:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer: swipeRightGestureRecognizer];
    
    /* passed both total price and discount price to view */
    ((MyQuartzView *)self.view).totalPrice = self.tempTotalPrice;
    ((MyQuartzView *)self.view).discountPrice = self.tempDiscountPrice;

}

- (void)handleSwipeRightFrom:(UIGestureRecognizer*)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    

}


@end
