/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the core graphics view that displays the bar chart 
 */

#import "MyQuartzView.h"

@implementation MyQuartzView

@synthesize totalPrice;
@synthesize discountPrice;

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"initWithFrame");
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clearsContextBeforeDrawing = YES;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code

    /* Font for labels */
    UIFont *textFont = [UIFont systemFontOfSize:13];
    
    UIColor * textColor = [UIColor colorWithRed:0.44 green:0.44 blue:0.44 alpha:1.0];
    
    /* Currency Formatter */
    /* Currency Formatter */
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setMinimumFractionDigits:2];
    
    
    /* All Variables necessary to draw the rectangles */
    float fullPriceX = 50.0;
    float fullPriceY = 120.0;
    float width = 80.0;
    float fullPriceHeight = 300.0;
    float savingsHeight = 0.0;
    float savingsOriginX = 190.0;
    float fullPriceRed = 0.23;
    float fullPriceGreen = 0.58;
    float fullPriceBlue = 0.82;
    float savingsRed = 0.23;
    float savingsGreen = 0.58;
    float savingsBlue = 0.06;
    float discountRed = 0.0;
    float discountGreen = 0.0;
    float discountBlue = 0.0;
    float discountOriginY = 0.0;
    float discountHeight = 0.0;
    float discountAlpha = 1.0;
    
    
    /* Get a content first */
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    /* Draw the underlying grid */
    CGContextSetRGBStrokeColor(context, 0.745, 0.745, 0.745, 0.8);
    
    CGContextSetLineWidth(context, 0.5);
    for (int i = 0; i < 430.0; i=i+30)
    {
        CGPoint addLines[] =
        {
            CGPointMake(0.0, (float)i),
            CGPointMake(360.0,  (float)i),
        };
        CGContextAddLines(context, addLines, sizeof(addLines)/sizeof(addLines[0]));
        CGContextStrokePath(context);
    }
    
    /* If there is no discountPrice input */
    if (discountPrice == NULL)
    {

        savingsHeight = fullPriceHeight;
        
        savingsRed = 0.745;
        savingsGreen = 0.745;
        savingsBlue = 0.745;
        
        fullPriceRed = 0.745;
        fullPriceGreen = 0.745;
        fullPriceBlue = 0.745;
        
        // Create Legend
        CGRect fullPriceSquareLegend = CGRectMake(19.0, 480.0, 15.0, 15.0);
        
        // Make fill color blue
        CGContextSetRGBFillColor(context, fullPriceRed, fullPriceGreen, fullPriceBlue, 1.0);
        
        // Add Rect to the current path/context, then stroke it
        CGContextFillRect(context, fullPriceSquareLegend);
        [@"No Calculations" drawAtPoint: CGPointMake(40.0, 480) withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:textColor}];

        
    }
    /* If the discountPrice is negative */
    else if ([discountPrice floatValue] <= 0 )
    {
        savingsRed = 0.58;
        savingsGreen = 0.486;
        savingsBlue = 0.725;
        savingsHeight = fullPriceHeight;
        discountOriginY = 120.0;
        discountHeight = 0.0;
        discountAlpha = 0.0;
        discountBlue = 0.0;
        discountGreen = 0.0;
        discountRed = 0.0;
        
        // Create Legend
        CGRect fullPriceSquareLegend = CGRectMake(19.0, 480.0, 15.0, 15.0);
        
        // Make fill color blue
        CGContextSetRGBFillColor(context, fullPriceRed, fullPriceGreen, fullPriceBlue, 1.0);
        
        // Add Rect to the current path/context, then stroke it
        CGContextFillRect(context, fullPriceSquareLegend);
        
        
        NSString * fullPrice = [NSString stringWithFormat:@"Total Price: %@", [currencyFormatter stringFromNumber:totalPrice]];
        [fullPrice drawAtPoint: CGPointMake(40.0, 480) withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:textColor}];
        
        /* Other square */
        CGRect discountSquareLegend = CGRectMake(19.0, 510.0, 15.0, 15.0);
        
        CGContextSetRGBFillColor(context, savingsRed, savingsGreen, savingsBlue, 1.0);
        
        // Add Rect to the current path/context, then stroke it
        CGContextFillRect(context, discountSquareLegend);
        
        /* Label to describe box */
        NSString * discountedPrice = [NSString stringWithFormat:@"Total Discount Price: %@ ", [currencyFormatter stringFromNumber:discountPrice]] ;
        [discountedPrice drawAtPoint: CGPointMake(40.0, 510.0) withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:textColor}];
        
    }
    else{
        
        savingsHeight = (1 -([discountPrice  floatValue] / [totalPrice floatValue])) *  fullPriceHeight;
        savingsRed = 0.58;
        savingsGreen = 0.788;
        savingsBlue = 0.4317;
        discountOriginY = fullPriceY + savingsHeight;
        discountHeight = fullPriceHeight - savingsHeight;
        discountRed = 0.925;
        discountGreen = 0.36;
        discountBlue = 0.317;
        
        // Create Legend
        CGRect fullPriceSquareLegend = CGRectMake(19.0, 480.0, 15.0, 15.0);
        
        // Make fill color blue
        CGContextSetRGBFillColor(context, fullPriceRed, fullPriceGreen, fullPriceBlue, 1.0);
        
        // Add Rect to the current path/context, then stroke it
        CGContextFillRect(context, fullPriceSquareLegend);
        
        
        NSString * fullPrice = [NSString stringWithFormat:@"Total Price: %@", [currencyFormatter stringFromNumber:totalPrice]];
        [fullPrice drawAtPoint: CGPointMake(40.0, 480) withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:textColor}];
        
        /* Other square */
        CGRect savingsSquareLegend = CGRectMake(19.0, 500.0, 15.0, 15.0);
        
        CGContextSetRGBFillColor(context, discountRed, discountGreen, discountBlue, 1.0);
        
        // Add Rect to the current path/context, then stroke it
        CGContextFillRect(context, savingsSquareLegend);
        
        /* Label to describe box */
        NSString * SavingsPrice = [NSString stringWithFormat:@"Total Savings: %@  %0.2f%%", [currencyFormatter stringFromNumber:[totalPrice decimalNumberBySubtracting:discountPrice]], ((1-([discountPrice  floatValue] / [totalPrice floatValue]))*100)] ;
        [SavingsPrice drawAtPoint: CGPointMake(40.0, 520.0) withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:textColor}];
        
        /* Other square */
        CGRect discountSquareLegend = CGRectMake(19.0, 520.0, 15.0, 15.0);
        
        CGContextSetRGBFillColor(context, savingsRed, savingsGreen, savingsBlue, 1.0);
        
        // Add Rect to the current path/context, then stroke it
        CGContextFillRect(context, discountSquareLegend);
        
        /* Label to describe box */
        NSString * discountedPrice = [NSString stringWithFormat:@"Total Discount Price: %@  %0.2f%%", [currencyFormatter stringFromNumber:discountPrice], (([discountPrice  floatValue] / [totalPrice floatValue])*100)] ;
        [discountedPrice drawAtPoint: CGPointMake(40.0, 500.0) withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:textColor}];
        
        
    }
    
    
    
    // Create the fullPrice rectangle
    CGRect fullPriceRectangle = CGRectMake(fullPriceX, fullPriceY, width, fullPriceHeight);
    
    // Make fill color blue
    CGContextSetRGBFillColor(context, fullPriceRed, fullPriceGreen, fullPriceBlue, 1.0);
    
    // Create rounded corners for the rectangle
    //[[UIBezierPath bezierPathWithRoundedRect:leftRect cornerRadius:8.0] addClip];
    
    // Add Rect to the current path/context, then stroke it
    CGContextFillRect(context, fullPriceRectangle);
    
    // Create the savings rectangle
    CGRect SavingsRectangle = CGRectMake(savingsOriginX, fullPriceY, width, savingsHeight);
    
    // Make fill color blue
    CGContextSetRGBFillColor(context, savingsRed, savingsGreen, savingsBlue, 1.0);
    
    // Create rounded corners for the rectangle
    //[[UIBezierPath bezierPathWithRoundedRect:rightRect cornerRadius:8.0] addClip];
    
    // Add Rect to the current path/context, then stroke it
    CGContextAddRect(context, SavingsRectangle);
    CGContextFillPath(context);
    
    // Create the savings rectangle
    CGRect DiscountRectangle = CGRectMake(savingsOriginX, discountOriginY, width, discountHeight);
    
    // Make fill color blue
    CGContextSetRGBFillColor(context, discountRed, discountGreen, discountBlue, 1.0);
    
    // Create rounded corners for the rectangle
    //[[UIBezierPath bezierPathWithRoundedRect:rightRect cornerRadius:8.0] addClip];
    
    // Add Rect to the current path/context, then stroke it
    CGContextAddRect(context, DiscountRectangle);
    CGContextFillPath(context);
    
    
    
    
}

@end
