/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the core graphics view that displays the bar chart
 */

#import <UIKit/UIKit.h>

@interface MyQuartzView : UIView

@property(weak, nonatomic) NSDecimalNumber * totalPrice;
@property(weak, nonatomic) NSDecimalNumber * discountPrice;



@end
