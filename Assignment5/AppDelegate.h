//
//  AppDelegate.h
//  Assignment5
//
//  Created by Noemi Quezada on 9/27/14.
//  Copyright (c) 2014 Noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
