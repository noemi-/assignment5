/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the nextTextfield attribute that is being added to UITextField
 */

#import <UIKit/UIKit.h>

@interface UITextField (NextTextFieldProperty)

@property (retain, nonatomic) UITextField* nextTextField;

@end
