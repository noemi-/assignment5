/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for implementing the priceTag model that is used to hold the data for each calculation, it returns original price and discount price
 */


#import "PriceTag.h"

static PriceTag * _priceTag;

@implementation PriceTag

@synthesize price;
@synthesize dollarsOff;
@synthesize discountPercent;
@synthesize additionalDiscount;
@synthesize tax;

+(instancetype)priceTag
{
    if (_priceTag == nil)
    {
        _priceTag = [[PriceTag alloc]init];
    }
    return _priceTag;
}

-(NSDecimalNumber *)originalPrice
{

    NSDecimalNumber * taxRate = [tax decimalNumberByMultiplyingByPowerOf10:(-2)];
    
    NSDecimalNumber * originalPrice =[price decimalNumberByAdding:[price decimalNumberByMultiplyingBy:taxRate]];
    
    return originalPrice;


}

-(NSDecimalNumber *)discountPrice
{
    /* Calculate discount rates */
    NSDecimalNumber * discountRate = [discountPercent decimalNumberByMultiplyingByPowerOf10:(-2)];
    NSDecimalNumber * extraDiscountRate = [additionalDiscount decimalNumberByMultiplyingByPowerOf10:(-2)];
    NSDecimalNumber * taxRate = [tax decimalNumberByMultiplyingByPowerOf10:(-2)];
    
    /* Calculate discountPrice */
    NSDecimalNumber * totalPrice = [price decimalNumberByAdding:[price decimalNumberByMultiplyingBy:taxRate]];
    NSDecimalNumber * firstDiscount = [price decimalNumberByMultiplyingBy:discountRate];
    NSDecimalNumber * secondDiscount = [[price decimalNumberBySubtracting:firstDiscount] decimalNumberByMultiplyingBy:extraDiscountRate];
    NSDecimalNumber * finalDiscountPrice = [[[totalPrice decimalNumberBySubtracting:firstDiscount] decimalNumberBySubtracting:secondDiscount] decimalNumberBySubtracting:dollarsOff];
    
    
    return finalDiscountPrice;
}
@end
