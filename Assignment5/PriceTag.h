/* Name: Noemi Quezada
 * Email: noemi01@csu.fullerton.edu
 * Description: This file is reponsible for defining the priceTag model that is used to hold the data for each calculation, it returns original price and discount price
 */


#import <Foundation/Foundation.h>

@interface PriceTag : NSObject

@property(nonatomic) NSDecimalNumber * price;
@property(nonatomic) NSDecimalNumber * dollarsOff;
@property(nonatomic) NSDecimalNumber * discountPercent;
@property (nonatomic) NSDecimalNumber * additionalDiscount;
@property (nonatomic) NSDecimalNumber * tax;
@property (readonly, nonatomic) NSDecimalNumber * originalPrice;
@property (readonly, nonatomic) NSDecimalNumber * discountPrice;

-(NSDecimalNumber *) originalPrice;
-(NSDecimalNumber *) discountPrice;
+(instancetype)priceTag;
@end
