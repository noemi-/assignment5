Name: Noemi Quezada
Email: noemi01@csu.fullerton.edu
Assignment: 5


Description: Assignment 5 Project : Discount Calculator ->
	Calculates the discount of a particular price inputted by the user,
 	A bar chart is created with the calculations output showing savings, discount
	and total price. 

Features: 
	Calculates Total Discount for US Currency.
	Inputs Dollars like a cash register (right to left)
	Creates a bar chart for the Total Discount, Total Price and Total Savings.
	Legend is provided to understand the Bar Chart
	Text fields limit number of characters that can be inputted to 8 so that
	it doesn’t overflow the textfield.
	Provides Keyboard Toolbar that allows to toggle between textfields and dismiss
	the keyboard.
	Provides a scrollable view just in case not all the textfields are in view 
	
	This application is for iPhone 5/5S (7.1)
	This application was built using Xcode 6.0.1

Run program: 
	1. Press the Play (Run) button at top left of screen
		make sure it is in iPhone 5/5s (7.1)
	2. Once Simulator opens up, input total price and any discounts and press Calculate.
	3. The application will calculate the total discount price and display the results. 


	
	
 